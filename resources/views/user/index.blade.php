@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <a href="{{ route('users.create') }}" class="btn btn-primary pull-right">
              Agregar usuario
            </a>
            <br><br>
            @if(isset($items))
            <div class="table-responsive">
              <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">Apellido</th>
                      <th scope="col">Email</th>
                      <th scope="col">Teléfono</th>
                      <th scope="col">Dirección</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($items->count())
                      @foreach ($items as $item)
                        <tr>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->lastname }}</td>
                          <td>{{ $item->email }}</td>
                          <td>{{ $item->phone }}</td>
                          <td>{{ $item->address }}</td>
                          <td>
                            <a href="{{ route('users.edit', $item->id) }}" class="btn btn-primary btn-sm">
                              Editar
                            </a>
                            <form method="POST" action="{{route('users.destroy', $item->id)}}">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger btn-sm" type="submit">
                                  Eliminar
                                </button>
                            </form>
                          </td>
                        </tr>
                      @endforeach
                    @else
                        <tr class="text-center"><td colspan="6">Sin registros</td></tr>
                    @endif
                  </tbody>
              </table>
            </div>
              <div style="float: right">
              {{ $items->appends(request()->except(['page','_token'])) }}
              </div>
            @endif
        </div>
    </div>
</div>
</div>
@endsection
