@extends('layouts.app')

@section('content')
  <div class="card">
    <div class="card-body">
      <h1>
      @if(isset($item->id))
        Actualizar usuario
      @else
        Crear usuario
      @endif
      </h1>

      @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

      <form action="{{$action}}" method="POST">
        {{ method_field($method) }}
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
              <div class="form-group">
                  <label for="title" class="control-label">
                    Nombre
                  </label>
                  <input id="name" name="name" type="text"
                    class="form-control" value="{{ old('name', $item->name) }}"
                    required>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
              <div class="form-group">
                  <label for="title" class="control-label">
                    Apellido
                  </label>
                  <input id="lastname" name="lastname" type="text"
                    class="form-control" value="{{ old('lastname', $item->lastname) }}"
                    required>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
              <div class="form-group">
                  <label for="title" class="control-label">
                    email
                  </label>
                  <input id="email" name="email" type="email"
                    class="form-control" value="{{ old('email', $item->email) }}"
                    required>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
              <div class="form-group">
                  <label for="title" class="control-label">
                    Teléfono
                  </label>
                  <input id="phone" name="phone" type="text"
                    class="form-control" value="{{ old('phone', $item->phone) }}">
              </div>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
              <div class="form-group">
                  <label for="title" class="control-label">
                    Dirección
                  </label>
                  <input id="address" name="address" type="text"
                    class="form-control" value="{{ old('address', $item->address) }}">
              </div>
            </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">
              @if(isset($item->id))
                Actualizar
              @else
                Crear
              @endif
          </button>
          <a href="{{ route('users.index') }}" class="btn btn-secondary">
              Volver
          </a>
        </div>
      </form>
    </div>
  </div>
@endsection
