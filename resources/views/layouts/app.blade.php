<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CRUD|usuarios</title>
    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet">
</head>
<body>
  <div class="content mt-3">
          <div class="col-sm-12">
              <div class="response"></div>
                @if (Session::has('success'))
                  <div class="alert alert-success">
                    {!! Session::get('success') !!}
                  </div>
                @endif
                @if (Session::has('error'))
                  <div class="alert alert-danger">
                    {!! Session::get('error') !!}
                  </div>
                @endif

              @yield('content')
          </div>
        </div>
</body>
</html>
