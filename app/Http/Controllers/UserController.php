<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    const ROW_BY_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = User::query();
        $items = $query->paginate(self::ROW_BY_PAGE);

        return view('user.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new User;
        $method = 'POST';
        $action = route('users.store');

        return view('user.createOrEdit', compact('item', 'action', 'method'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'name' => 'required|string',
           'email' => 'required|string|email|max:100|unique:users,email'
        ]);

        if ($validator->fails()) {
           return back()->withErrors($validator)->withInput();
        }

        $item = User::create($request->all());

        return redirect('/')->with('success', 'Usuario creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $item = User::find($id);
      $method = 'PUT';
      $action = route('users.update', $id);

      return view('user.createOrEdit', compact('item', 'action', 'method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
           'name' => 'required|string',
           'email' => 'required|string|email|max:100|unique:users,email,'.$id.',id'
        ]);

        if ($validator->fails()) {
           return back()->withErrors($validator)->withInput();
        }

        $item = User::find($id);

        $item->update($request->all());

        return redirect('/')->with('success', 'Usuario actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = User::find($id);
        $item->delete();

        return redirect('/')->with('success', 'Usuario eliminado exitosamente');
    }
}
